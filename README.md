# Salt formula for Tiny Tiny RSS installation

## What does it do?

This formula:

- installs [Tiny Tiny RSS](https://tt-rss.org/) with all its dependencies (PostgreSQL, PHP…)
- configures it
- installs and configures Nginx to serve Tiny Tiny RSS
- creates a [Let’s encrypt](https://letsencrypt.org/) certificate and configures Nginx to use it
- creates a cron task to warn you about the certificate expiration (even if the certificate should be automatically renewed)
- sets up a backup system (with [borg](https://borgbackup.readthedocs.io/) and `pg_dump`)
- can install and configure [`apticron`](https://packages.debian.org/stretch/apticron)

## How to use it?

Put `salt/ttrss/` into your Salt formulas directory, use <https://fiat-tux.frama.io/hat-softwares/salt/ttrss> to create the needed pillar configuration, put it into your pillar configuration, then you’re good to go.

## How to use it with salt-ssh?

All commands have to be run with the `root` user.

Install `salt-ssh`:
```
apt install salt-ssh
```

Modify `/etc/salt/roster`:
```
# You will need to authorize root to use SSH
# For security, authorize it only with SSH keys (PermitRootLogin prohibit-password in /etc/ssh/sshd_config)
my_server:
  host: serveur.example.org
  user: root
# Alternatively, you can use a regular user with sudo privilege
#my_server:
#  host: serveur.example.org
#  user: foo
#  sudo: True
```

Let `salt-ssh` creates a pair of SSH keys (answer no to the question that will be asked):
```
salt-ssh my_server test.ping
```

Put `/var/lib/salt/pki/master/ssh/salt-ssh.rsa.pub` content in `/root/.ssh/authorized_keys` (or `~.ssh/authorized_keys` of the user you choose)

Copy the formula
```
cp -r salt pillar /srv/
```

Use <https://fiat-tux.frama.io/salt/ttrss> to create the needed pillar, put it into your pillar configuration, then you’re good to go.

```
salt-ssh my_server state.sls ttrss
```

## Translating

If you want to translate the pillar generator in your language, you can do it on <https://trad.framasoft.org/iteration/view/salt/ttrss>.
Please, read its [homepage](https://trad.framasoft.org/) to know how to do it.
If you want to add a non-listed language on zanata, please create an issue on <https://framagit.org/fiat-tux/salt/ttrss/issues>.
