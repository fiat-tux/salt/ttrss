msgid ""
msgstr ""
"Project-Id-Version: i18next-conv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"POT-Creation-Date: 2019-05-06T20:43:10.301Z\n"
"PO-Revision-Date: 2019-05-06T20:43:10.301Z\n"
"Language: en\n"

msgid "Tiny Tiny RSS pillar generator"
msgstr "Tiny Tiny RSS pillar generator"

msgid "Created by {{- opentag}}Luc Didry{{- endtag}}"
msgstr "Created by {{- opentag}}Luc Didry{{- endtag}}"

msgid "Source"
msgstr "Source"

msgid "Language"
msgstr "Language"

msgid "Your instance"
msgstr "Your instance"

msgid ""
"Web address of your Tiny Tiny RSS instance (without {{- "
"opentag}}http(s)://{{- endtag}})"
msgstr ""
"Web address of your Tiny Tiny RSS instance (without {{- "
"opentag}}http(s)://{{- endtag}})"

msgid "How many CPU threads should be allocated to the update daemon?"
msgstr "How many CPU threads should be allocated to the update daemon?"

msgid "Setting up email sending"
msgstr "Setting up email sending"

msgid "Display name of the email’s sender"
msgstr "Display name of the email’s sender"

msgid "Email sender address"
msgstr "Email sender address"

msgid "Subject of digest emails"
msgstr "Subject of digest emails"

msgid "Registrations"
msgstr "Registrations"

msgid "Open registrations?"
msgstr "Open registrations?"

msgid ""
"Maximum number of accounts (when this number is reached, registrations are "
"disabled. For an unlimited number of accounts, put {{- opentag}}0{{- "
"endtag}})"
msgstr ""
"Maximum number of accounts (when this number is reached, registrations are "
"disabled. For an unlimited number of accounts, put {{- opentag}}0{{- "
"endtag}})"

msgid "Admin account"
msgstr "Admin account"

msgid ""
"The login of the admin account is, without surprise, {{- opentag}}admin{{- "
"endtag}}."
msgstr ""
"The login of the admin account is, without surprise, {{- opentag}}admin{{- "
"endtag}}."

msgid "Email attached to admin account"
msgstr "Email attached to admin account"

msgid "Password of admin account"
msgstr "Password of admin account"

msgid ""
"Apticron is a program that runs daily on your server and notifies you by "
"email if package updates are available."
msgstr ""
"Apticron is a program that runs daily on your server and notifies you by "
"email if package updates are available."

msgid "Install apticron? {{- opentag}}recommended{{- endtag}}"
msgstr "Install apticron? {{- opentag}}recommended{{- endtag}}"

msgid ""
"Recipient for apticron’s emails ({{- opentag}}root{{- endtag}} is ok if you "
"configured a real address in {{- opentag}}/etc/aliases{{- endtag}}"
msgstr ""
"Recipient for apticron’s emails ({{- opentag}}root{{- endtag}} is ok if you "
"configured a real address in {{- opentag}}/etc/aliases{{- endtag}}"

msgid "Download configuration"
msgstr "Download configuration"

msgid "Reset"
msgstr "Reset"