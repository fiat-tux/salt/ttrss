# vim:set sw=2 ts=2 sts=2 ft=yaml expandtab:
{% set settings = pillar['ttrss'][grains['id']] %}
{% set php_version = '7.0' %}
{% if grains['oscodename'] == 'buster' %}
  {% set php_version = '7.3' %}
{% endif %}
##################
## Dependencies ##
##################
dependencies:
  pkg.installed:
    - pkgs:
      - borgbackup
      - certbot
      - curl
      - git
      - jq
      - nginx
      - php-cli
      - php-curl
      - php-fpm
      - php-gd
      - php-intl
      - php-json
      - php-mbstring
      - php-pgsql
      - php-xml
      - php{{ php_version }}-opcache
      - postgresql

###################
## User creation ##
###################
ttrss_user:
  user.present:
    - name: ttrss
    - home: /var/www/ttrss
    - createhome: False
    - empty_password: True
    - system: True
  alias.present:
    - target: {{ settings.admin.email }}
  file.append:
    - name: /etc/ssh/sshd_config
    - text: DenyUsers ttrss
  service.running:
    - name: ssh
    - enable: True
    - watch:
      - file: /etc/ssh/sshd_config

#########################
## PostgreSQL database ##
#########################
ttrss:
  postgres_user.present:
    - password: {{ settings.db_password }}
postgresql_db:
  postgres_database.present:
    - name: ttrss_prod
    - owner: ttrss

# PostgreSQL extensions
postgresql_pg_trgm:
  postgres_extension.present:
    - name: pg_trgm
    - user: postgres
    - maintenance_db: ttrss_prod

#############
## PHP-FPM ##
#############
ttrss.conf:
  file.managed:
    - name: /etc/php/{{ php_version }}/fpm/pool.d/ttrss.conf
    - source: salt://ttrss/files/php.conf
  service.running:
    - name: php{{ php_version }}-fpm
    - enable: True
    - watch:
      - file: /etc/php/{{ php_version }}/fpm/pool.d/ttrss.conf

###################
## Install TTRSS ##
###################
clone_ttrss:
  git.cloned:
    - name: https://tt-rss.org/git/tt-rss.git
    - target: /var/www/ttrss
  file.directory:
    - name: /var/www/ttrss
    - user: ttrss
    - group: www-data
    - recurse:
      - user
      - group

#########################
## Configure Webserver ##
#########################
/etc/nginx/sites-available/ttrss:
  file.managed:
    - source: salt://ttrss/files/nginx.conf.sls
    - template: jinja
    - defaults:
        settings: {{ settings }}
/etc/nginx/sites-enabled/ttrss:
  file.symlink:
    - target: /etc/nginx/sites-available/ttrss
  service.running:
    - name: nginx
    - enable: True
    - reload: True
    - watch:
      - file: /etc/nginx/sites-enabled/ttrss
letsencrypt:
  cmd.run:
    - name: certbot certonly --non-interactive --rsa-key-size 4096 --webroot -w /var/www/certbot/ --email {{ settings.letsencrypt_email|default(settings.admin.email) }} --agree-tos --text --renew-hook "/usr/sbin/nginx -s reload" -d {{ settings.hostname }}
  file.uncomment:
    - name: /etc/nginx/sites-available/ttrss
    - regex: ^#
  service.running:
    - name: nginx
    - enable: True
    - reload: True
    - watch:
      - file: /etc/nginx/sites-available/ttrss

#####################
## Configure TTRSS ##
#####################
salt://ttrss/files/install.sh:
  cmd.script:
    - shell: /bin/bash
    - template: jinja
    - defaults:
      settings: {{ settings }}

######################################
## Put TTRSS logs in /var/log/ttrss ##
######################################
/var/log/ttrss:
  file.directory:
    - user: root
    - group: adm
    - mode: 750
/etc/logrotate.d/ttrss.conf:
  file.managed:
    - source: salt://ttrss/files/logrotate.conf
/etc/rsyslog.d/ttrss.conf:
  file.managed:
    - contents:
      - if $programname == 'ttrss' then /var/log/ttrss/ttrss.log
      - if $programname == 'ttrss' then ~
  service.running:
    - name: rsyslog
    - enable: True
    - watch:
      - file: /etc/rsyslog.d/ttrss.conf

##################################
## Setting up the update daemon ##
##################################
/etc/systemd/system/ttrss.service:
  file.managed:
    - source: salt://ttrss/files/ttrss.service.sls
    - template: jinja
    - defaults:
        settings: {{ settings }}
  cmd.run:
    - name: systemctl daemon-reload
  service.running:
    - name: ttrss
    - enable: True
    - watch:
      - file: /etc/systemd/system/ttrss.service

############################
## Certificate monitoring ##
############################
certificate_monitoring:
  pkg.installed:
    - name: monitoring-plugins-basic
  cron.present:
    - name: /usr/lib/nagios/plugins/check_http -H {{ settings.hostname }} -C 25 --sni -p 443 -t 15 | grep -v OK
    - user: ttrss
    - minute: 0
    - hour: 6
    - comment: Check for the TTRSS certificate expiration delay. If warning, try to reload nginx. If still warning, do 'certbot renew'

############
## Backup ##
############
/var/www/ttrss_daily_backups:
  file.directory:
    - dir_mode: 755
    - user: root

# PostgreSQL database
/var/www/ttrss_daily_backups/postgresql:
  file.directory:
    - dir_mode: 750
    - user: postgres

# Backup with borg
borgbackup:
  file.directory:
    - name: /var/www/ttrss_daily_backups/borg
    - dir_mode: 750
  cmd.run:
    - name: borg init -e none /var/www/ttrss_daily_backups/borg/
backup_script:
  file.managed:
    - name: /var/www/ttrss_daily_backups/backup-ttrss.sh
    - source: salt://ttrss/files/backup.sh
    - user: root
    - mode: 750
  cron.present:
    - name: /var/www/ttrss_daily_backups/backup-ttrss.sh
    - user: root
    - comment: Backup TTRSS on borg
    - minute: 42
    - hour: 1

{% if settings.apticron is defined and settings.apticron.enabled%}
#############################################
## Let's try to keep the server up to date ##
#############################################
apticron:
  pkg.installed
/etc/apticron/apticron.conf:
  file.replace:
    - name: /etc/apticron/apticron.conf
    - pattern: EMAIL="root"
    - repl: EMAIL="{{ settings.apticron.email }}"
{% endif %}
