# vim:set sw=2 ts=2 sts=2 ft=yaml expandtab:
ttrss:
  my_server:
    db_password: ''
    apticron:
      enabled: false
      email: root
    hostname: ttrss.example.com
    threads: 1
    admin:
      email: admin@example.com
      password: totototo
    registration:
      activated: false
      accounts_number: 10
    smtp:
      from_name: Tiny Tiny RSS
      from_address: noreply@example.com
      digest_subject: '[tt-rss] New headlines for last 24 hours'
